$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 5000
    });

 
    $('#contactoBtn').on('show.bs.modal', function(e){
        console.log('el modal contacto se esta mostrando');
        $('#contactoBtn').removeClass('btn-primary');
        $('#contactoBtn').addClass('btn-outline-success');
        $('#contactoBtn').prop('disabled', true);
    });
    $('#contactoBtn').on('shown.bs.modal', function(e){
        console.log('el modal contacto se mostró');
    });
    $('#contactoBtn').on('hide.bs.modal', function(e){
        console.log('el modal contacto se está ocultando');
    });
    $('#contactoBtn').on('hidden.bs.modal', function(e){
        console.log('el modal contacto se esta ocultó');
        $('#contactoBtn').prop('disabled', false);

    });
    
});